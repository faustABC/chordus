import type { Preview } from '@storybook/react';
import { ThemeProvider, CssBaseline } from '@mui/material';
import { withThemeFromJSXProvider } from '@storybook/addon-themes';
import theme from '../src/utils/theme';
import { MUIProvider } from '../src/providers/MUIProvider';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
};

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};

export const decorators = [
  withThemeFromJSXProvider({
    GlobalStyles: CssBaseline,
    Provider: MUIProvider,
    themes: {
      light: theme,
    },
    defaultTheme: 'light',
  }),
];

export default preview;
