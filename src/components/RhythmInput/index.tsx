'use client';

import React, {
  ChangeEvent,
  FC,
  FormEvent,
  HTMLAttributes,
  useCallback,
  useMemo,
  useRef,
} from 'react';
import { produce } from 'immer';
import { Box, TextField, useTheme } from '@mui/material';

export const RhythmInput: FC<{
  rhythm: [number, number];
  maxMeasure?: number;
  setRhythm?: React.Dispatch<React.SetStateAction<[number, number]>>;
  isEnabled?: boolean;
}> = ({ maxMeasure = 32, rhythm, setRhythm, rootProps, isEnabled = false }) => {
  const theme = useTheme();

  const handleChange = useCallback(
    (rhythmIndex: number) => (e: ChangeEvent<HTMLInputElement>) => {
      const numberValue = Number(e.target.value);
      if (numberValue > maxMeasure || numberValue < 0) return;
      setRhythm?.(
        produce(rhythm => {
          rhythm[rhythmIndex] = numberValue;
        }),
      );
    },
    [maxMeasure, setRhythm],
  );

  return (
    <Box sx={{ display: 'flex', gap: 1, flexDirection: 'column' }}>
      {rhythm.map((_item, index) => (
        <TextField
          key={index}
          sx={{
            ...theme.typography.body2,
            width: theme => theme.spacing(16),
          }}
          size="small"
          onChange={handleChange(index)}
          value={rhythm[index]}
          type="number"
          inputMode="numeric"
          inputProps={{
            pattern: '[0-9]*',
            min: 1,
            max: 64,
          }}
        />
      ))}
    </Box>
  );
};
