import { FC, PropsWithChildren } from 'react';
import { defaultStyle, duration, transitionStyles } from './styles';
import { Transition } from 'react-transition-group';

export const Fade: FC<{ in: boolean } & PropsWithChildren> = ({
  in: inProp,
  children,
}) => (
  <Transition in={inProp} timeout={duration}>
    {state => (
      <div
        style={{
          ...defaultStyle,
          ...transitionStyles[state as keyof typeof transitionStyles],
        }}
      >
        {children}
      </div>
    )}
  </Transition>
);
