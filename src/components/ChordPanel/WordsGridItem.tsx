import { clsx } from 'clsx';
import React, { FC, PropsWithChildren } from 'react';
import { ChordAlign } from 'types';
import styles from './index.module.scss';
import { useDrop } from 'react-dnd';
import { DndTypes } from '../../constant';
import { DragObject, CollectWordDragProps } from './types';
import commonStyles from 'styles/common.module.scss';

export const WordsGridItem: FC<
  {
    lineIndex: number;
    tactIndex: number;
    chordIndex: number;
    showSetChordForm: boolean;
    setChordForWord: (wordIndex: number, chordAlign: ChordAlign) => void;
    setChordFormAlign: React.Dispatch<
      React.SetStateAction<Pick<ChordAlign, 'tactIndex' | 'chordIndex'> | null>
    >;
    setChordFormWordIndex: React.Dispatch<
      React.SetStateAction<number | undefined>
    >;
    isLast: boolean;
    onActive: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    isEnabled: boolean;
  } & PropsWithChildren
> = ({
  lineIndex,
  tactIndex,
  chordIndex,
  setChordForWord,
  children,
  setChordFormAlign,
  showSetChordForm,
  setChordFormWordIndex,
  isLast,
  onActive,
  isEnabled,
}) => {
  const [{ isOver, canDrop, draggedItem }, drop] = useDrop<
    DragObject,
    void,
    CollectWordDragProps
  >({
    accept: DndTypes.CHORD_ELEMENT,
    drop: item => {
      setChordForWord(item.wordAlign.wordIndex, {
        lineIndex,
        tactIndex,
        chordIndex,
      });
      if (showSetChordForm) {
        setChordFormWordIndex(item.wordAlign.wordIndex);
        setChordFormAlign({ chordIndex, tactIndex });
      }
    },
    canDrop: item => item.wordAlign.lineIndex === lineIndex,
    collect: monitor => ({
      isOver: !!monitor.isOver(),
      canDrop: monitor.canDrop(),
      draggedItem: monitor.getItem(),
    }),
  });

  return (
    <div
      aria-hidden="true"
      ref={drop}
      onClick={onActive}
      className={clsx(styles.wordsGridItem, {
        [styles.isOverWordsGridItem]: isOver && canDrop,
        [styles.canDropWordsGridItem]: draggedItem && canDrop,
        [styles.firstWordsOnLine]: chordIndex === 0 && tactIndex === 0,
        [styles.lastWordsOnLine]: isLast,
        [commonStyles.disabled]: !isEnabled,
      })}
    >
      {children}
    </div>
  );
};
