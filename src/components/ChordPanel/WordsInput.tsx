import { clsx } from 'clsx';
import { ChangeEvent, FocusEvent, useCallback, useState } from 'react';
import styles from './index.module.scss';

export const WordsInput = ({
  spanUntil,
  onSubmit,
  spanFrom,
  placeholder,
  initialText,
  isLastColumn,
  isFirstColumn,
  isEnabled,
}: {
  spanUntil: number;
  spanFrom: number;
  onSubmit: (text: string) => void;
  placeholder?: string;
  initialText?: string;
  isLastColumn?: boolean;
  isFirstColumn?: boolean;
  isEnabled?: boolean;
}) => {
  const [text, setText] = useState(initialText ?? '');
  const onChangeText = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
  }, []);
  const submitHandler = (e: FocusEvent<HTMLFormElement>) => {
    e.preventDefault();
    onSubmit(text);
  };

  return (
    <form
      onBlur={submitHandler}
      onSubmit={submitHandler}
      className={clsx(
        styles.inputWords,
        styles.wordsGridItem,
        isLastColumn && styles.lastWordsOnLine,
        isFirstColumn && styles.firstWordsOnLine,
      )}
      style={{ gridColumn: `${spanFrom} / ${spanUntil}` }}
    >
      <span className={styles.inputWordsSpanWrapper}>
        <span className={styles.widthMachine} aria-hidden="true">
          {text || ' '}
        </span>
        {isEnabled ? (
          <input
            value={text}
            onChange={onChangeText}
            placeholder={placeholder}
          />
        ) : (
          <span>{text}</span>
        )}
      </span>
    </form>
  );
};
