import { DndTypes } from 'constant';

export interface CollectWordDragProps {
  isOver: boolean;
  canDrop: boolean;
  draggedItem: Record<string, unknown>;
}

export type WordAlign = { lineIndex: number; wordIndex: number };

export type DragObject = { type: DndTypes.CHORD_ELEMENT; wordAlign: WordAlign };
