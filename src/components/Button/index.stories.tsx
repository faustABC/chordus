import { LoadingButton } from '@mui/lab';
import { Meta, StoryObj } from '@storybook/react';

const meta: Meta<typeof LoadingButton> = {
  component: LoadingButton,
  tags: ['autodocs'],
};

export default meta;

type Story = StoryObj<typeof LoadingButton>;

export const LargeContained: Story = {
  args: {
    size: 'large',
    children: 'large contained',
    variant: 'contained',
    loading: false,
  },
};

export const LargeContainedDisabled: Story = {
  args: {
    size: 'large',
    children: 'large contained',
    variant: 'contained',
    disabled: true,
  },
};

export const LargeOutlined: Story = {
  args: {
    size: 'large',
    children: 'large outlined',
    variant: 'outlined',
  },
};

export const LargeOutlinedDisabled: Story = {
  args: {
    size: 'large',
    children: 'large outlined',
    variant: 'outlined',
    disabled: true,
  },
};

export const SmallContained: Story = {
  args: {
    size: 'small',
    children: 'small contained',
    variant: 'contained',
  },
};

export const SmallContainedDisabled: Story = {
  args: {
    size: 'small',
    children: 'small contained',
    variant: 'contained',
    disabled: true,
  },
};

export const SmallOutlined: Story = {
  args: {
    size: 'small',
    children: 'small outlined',
    variant: 'outlined',
  },
};

export const SmallOutlinedDisabled: Story = {
  args: {
    size: 'small',
    children: 'small outlined',
    variant: 'outlined',
    disabled: true,
  },
};
