import { Meta, StoryObj } from '@storybook/react';
import { GlobalSearch } from 'components/GlobalSearch';

const meta: Meta = {
  component: GlobalSearch,
  title: 'Components/GlobalSearch',
};

export default meta;

type Story = StoryObj<typeof GlobalSearch>;

export const Default: Story = {
  args: {},
};
