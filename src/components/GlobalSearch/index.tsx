'use client';

import React, {
  FC,
  SyntheticEvent,
  useState,
  forwardRef,
  useRef,
  ReactNode,
} from 'react';
import { useBoolean, useDebounce } from 'utils/hooks';
import api from 'api';
import { useRouter } from 'next/navigation';
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import {
  Box,
  FormControl,
  InputBase,
  MenuItem,
  Typography,
  Link as MUILink,
  AutocompleteProps,
} from '@mui/material';
import Link from 'next/link';
import theme from 'utils/theme';
import SearchIcon from '@/public/icons/search.svg';
import CloseIcon from '@/public/icons/close.svg';
import {
  createSuggestionUrl,
  getSuggestionData,
  isPerformer,
  isSong,
  PerformerSuggestion,
  searchDtoToSuggestions,
  SongSuggestion,
  Suggestion,
} from 'components/GlobalSearch/utils';

interface SuggestionItemProps {
  title?: string;
  href: string;
}

const SuggestionItem: FC<SuggestionItemProps> = forwardRef<
  HTMLLIElement,
  SuggestionItemProps
>(function SuggestionItem({ title, href, ...rest }, ref) {
  return (
    <MUILink
      passHref
      component={Link}
      href={href}
      sx={{
        textDecoration: 'none',
        ':not(:last-child) div': {
          borderBottom: theme => '1px solid ' + theme.palette.secondary.main,
        },
      }}
    >
      <MenuItem ref={ref} {...rest} style={{ padding: 0 }}>
        <Box
          sx={{
            padding: '12px 16px',
            mx: 4,
            width: '100%',
          }}
        >
          <Typography variant="body1">{title}</Typography>
        </Box>
      </MenuItem>
    </MUILink>
  );
});

export const GlobalSearch: FC = () => {
  const [suggestions, setSuggestions] = useState<Suggestion[]>([]);
  const [isOpen, { on: open, off: close }] = useBoolean();
  const [isLoading, { on: startLoading, off: stopLoading }] = useBoolean();
  const rootRef = useRef<HTMLDivElement>(null);
  const router = useRouter();
  const [lastSearchedText, setLastSearchedText] = useState<
    string | undefined
  >();
  const onSearch = useDebounce(async (text: string) => {
    startLoading();
    const data = await api.search.globalSearch(text);
    setSuggestions(searchDtoToSuggestions(data));
    stopLoading();
    setLastSearchedText(text);
  }, 250);

  const onInputChange = (
    _event: SyntheticEvent<Element, Event>,
    text: string,
  ) => {
    const trimmedText = text.trim();
    if (trimmedText.length === 0) setSuggestions([]);
    if (trimmedText === lastSearchedText || trimmedText.length <= 2) return;
    onSearch(trimmedText);
  };

  const renderOption = (
    props: React.HTMLAttributes<HTMLLIElement>,
    option: Suggestion,
  ) => {
    const [title, url, id] = getSuggestionData(option);

    return (
      <SuggestionItem
        key={id}
        title={title}
        href={createSuggestionUrl(url, id)}
        {...props}
      />
    );
  };

  const renderInput: AutocompleteProps<
    unknown,
    false,
    false,
    true,
    'div'
  >['renderInput'] = ({
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    InputLabelProps: _InputLabelProps,
    InputProps,
    inputProps,
    ...rest
  }) => {
    return (
      <FormControl {...rest}>
        <Box
          ref={rootRef}
          sx={theme => ({
            '.Mui-focused &': {
              border: `1px solid ${theme.palette.primary.main}`,
              ...(isOpen
                ? {
                    borderBottom: 'unset',
                  }
                : {}),
            },
            px: theme.spacing(5),
            width: '100%',
            boxSizing: 'border-box',
            border: `1px solid ${theme.palette.secondary.main}`,
            borderRadius: '24px',
            display: 'flex',
            alignItems: 'center',
            ...(isOpen
              ? {
                  borderBottomLeftRadius: 0,
                  borderBottomRightRadius: 0,
                  borderBottom: 'unset',
                }
              : {}),
          })}
        >
          {isLoading ? (
            <CircularProgress
              thickness={3}
              size={17}
              sx={{
                marginRight: '4.5px',
                marginLeft: '2.5px',
                marginBottom: '2px',
                transform: 'translate(-10px)',
              }}
            />
          ) : (
            <SearchIcon sx={{ mx: 1, fill: theme.palette.primary.main }} />
          )}
          <InputBase
            {...InputProps}
            sx={{
              borderRadius: theme => theme.spacing(10),
              flexGrow: 1,
              '& > input::placeholder': {
                opacity: 0.7,
              },
            }}
            placeholder="Search for song or artist"
            margin="dense"
            inputProps={{
              ...inputProps,
              style: {
                padding: 12,
              },
            }}
            size="small"
          />
        </Box>
      </FormControl>
    );
  };

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        position: 'relative',
      }}
    >
      <Autocomplete
        id="search-input"
        onOpen={open}
        open={isOpen}
        onClose={close}
        groupBy={option => option.type}
        PopperComponent={({ children }) => (
          <Box sx={{ position: 'absolute', top: '100%', left: 0, right: 0 }}>
            {children as ReactNode}
          </Box>
        )}
        PaperComponent={({ children, ...props }) =>
          isOpen && (
            <Box
              {...props}
              sx={{
                height: 'calc(100vh - 70px)',
              }}
            >
              {children}
              {!suggestions.length && (
                <Box
                  sx={{
                    height: '100%',
                    position: 'relative',
                  }}
                >
                  <Typography
                    sx={{
                      color: theme.palette.primary.main,
                      position: 'absolute',
                      top: '50%',
                      left: '50%',
                      transform: 'translate(-50%, -50%)',
                    }}
                  >
                    No recent searches
                  </Typography>
                </Box>
              )}
            </Box>
          )
        }
        ListboxProps={{
          sx: {
            maxHeight: 'unset',
          },
        }}
        componentsProps={{
          paper: {
            sx: {
              background: 'white',
              borderBottomLeftRadius: 16,
              borderBottomRightRadius: 16,
              borderTopRightRadius: 0,
              borderTopLeftRadius: 0,
              border: `1px solid ${theme.palette.primary.main}`,
              borderTop: 'none',
              boxShadow: 'none',
              '::after': {
                content: '""',
                top: 0,
                position: 'absolute',
                right: theme.spacing(8),
                left: theme.spacing(4),
                borderTop: `1px solid ${theme.palette.secondary.main}`,
              },
            },
          },
        }}
        renderGroup={param => {
          return (
            <li key={param.key}>
              <Typography
                sx={{
                  ml: theme => theme.spacing(8),
                  mb: theme => theme.spacing(2),
                  mt: theme => theme.spacing(5),
                  fontWeight: 600,
                  color: theme.palette.primary.main,
                  textTransform: 'capitalize',
                }}
                variant="h6"
              >
                {param.group}
              </Typography>
              <ul style={{ padding: 0 }}>{param.children}</ul>
            </li>
          );
        }}
        clearIcon={
          <CloseIcon
            style={{
              width: 20,
              height: 20,
              transform: 'translate(-2px, -2px)',
            }}
          />
        }
        autoHighlight
        freeSolo
        filterOptions={x => x}
        autoComplete
        getOptionLabel={option => {
          if (isPerformer(option as Suggestion)) {
            return (option as PerformerSuggestion).bandName;
          }
          if (isSong(option as Suggestion)) {
            return (option as SongSuggestion).songTitle;
          }
          return '';
        }}
        renderOption={renderOption}
        options={suggestions}
        onInputChange={onInputChange}
        onChange={(_event, option) => {
          const [, url, id] = getSuggestionData(option as Suggestion);
          router.push(createSuggestionUrl(url, id));
          close();
        }}
        renderInput={renderInput}
      />
    </Box>
  );
};
