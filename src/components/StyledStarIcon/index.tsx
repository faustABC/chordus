import StarIcon from '@/public/icons/star.svg';
import styled from '@emotion/styled';

export const StyledStarIcon = styled(StarIcon)({
  fill: 'none',
  path: {
    stroke: '#737373',
  },
});
