'use client';

import { Button, ButtonGroup } from '@mui/material';
import { useActor } from '@xstate/react';
import { GlobalStateContext } from 'providers/StateProvider';
import { useTopChordsSelectedQuery } from 'queries/common';
import { FC, useContext } from 'react';
import { TopVariant } from 'screens/TopChords/types';

export const TopSelector: FC = () => {
  const { isAnyLoading } = useTopChordsSelectedQuery();
  const globalServices = useContext(GlobalStateContext);
  const [topChordsState, sendTopChords] = useActor(globalServices.topChords);
  const { selectedTopVariant } = topChordsState.context;

  return (
    <ButtonGroup
      disabled={isAnyLoading}
      variant="outlined"
      aria-label="outlined primary button group"
    >
      {[
        { label: 'Songs', topVariant: TopVariant.chord },
        { label: 'Performers', topVariant: TopVariant.performer },
      ].map(({ label, topVariant }, index) => (
        <Button
          variant={selectedTopVariant === topVariant ? 'contained' : undefined}
          onClick={() =>
            sendTopChords({
              type: 'SET_TOP_VARIANT',
              topVariant,
            })
          }
          key={index}
        >
          {label}
        </Button>
      ))}
    </ButtonGroup>
  );
};
