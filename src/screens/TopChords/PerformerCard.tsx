import { FC } from 'react';
import { fallbackAvatar } from '../../constant';
import {
  Avatar,
  Card,
  CardActionArea,
  CardActions,
  CardHeader,
} from '@mui/material';
import Image from 'next/image';
import FavoriteIcon from '@mui/icons-material/Favorite';
import VisibilityIcon from '@mui/icons-material/Visibility';

export interface PerformerCardProps {
  performerImageUrl: string;
  performerBandName: string;
  popularSongTitle: string;
  rating: number;
  views: number;
  onClick: () => void;
}

export const PerformerCard: FC<PerformerCardProps> = ({
  performerImageUrl,
  popularSongTitle,
  performerBandName,
  rating,
  views,
  onClick,
}) => (
  <Card>
    <CardActionArea onClick={onClick}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: 'secondary.main' }} aria-label="recipe">
            {performerImageUrl && (
              <Image
                alt="Performer"
                src={performerImageUrl || fallbackAvatar}
                width={50}
                height={50}
              />
            )}
            {performerBandName.substr(0, 1).toUpperCase()}
          </Avatar>
        }
        title={performerBandName}
        subheader={popularSongTitle}
      />
      <CardActions>
        {!!rating && (
          <>
            <FavoriteIcon />
            {rating.toFixed(1)}
          </>
        )}
        {!!views && (
          <>
            <VisibilityIcon />
            {views}
          </>
        )}
      </CardActions>
    </CardActionArea>
  </Card>
);
