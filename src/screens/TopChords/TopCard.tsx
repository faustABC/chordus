import { FC } from 'react';
import {
  Avatar,
  Box,
  Card,
  CardActionArea,
  CardHeader,
  Skeleton,
  Typography,
} from '@mui/material';
import Image from 'next/image';
import EyeIcon from '@/public/icons/eye.svg';
import { StyledStarIcon } from 'components/StyledStarIcon';
import CameraIcon from '@/public/icons/camera.svg';

interface TopCardProps {
  avatarImageUrl: string;
  subheader: string;
  title: string;
  rating: number;
  views: number;
  onClick: () => void;
  isLoading?: boolean;
}

export const TopCard: FC<TopCardProps> = ({
  avatarImageUrl,
  title,
  subheader,
  rating,
  views,
  onClick,
  isLoading = false,
}) => (
  <Card sx={{ bgcolor: 'white', borderRadius: '12px' }} variant="outlined">
    <CardActionArea onClick={onClick} disabled={isLoading}>
      <CardHeader
        avatar={
          !isLoading ? (
            <Avatar
              sx={{
                bgcolor: theme => theme.palette.common.white,
                height: 60,
                width: 60,
                border: theme => `1px solid ${theme.palette.secondary.main}`,
                textTransform: 'capitalize',
                color: theme => theme.palette.secondary.main,
              }}
              aria-label="recipe"
            >
              {avatarImageUrl ? (
                <Image
                  alt="Performer avatar"
                  src={avatarImageUrl}
                  width={60}
                  height={60}
                />
              ) : (
                <Box
                  sx={{ g: { stroke: theme => theme.palette.primary.main } }}
                >
                  <CameraIcon />
                </Box>
              )}
            </Avatar>
          ) : (
            <Skeleton variant="circular" width={40} height={40} />
          )
        }
        title={
          !isLoading ? (
            <Typography variant="h5" sx={{ fontWeight: 600 }}>
              {title}
            </Typography>
          ) : (
            <Skeleton variant="text" />
          )
        }
        subheader={
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            {!isLoading ? (
              <Typography variant="body1">{subheader}</Typography>
            ) : (
              <Skeleton variant="text" />
            )}
            <Box sx={{ display: 'flex', columnGap: 3, alignItems: 'center' }}>
              {!isLoading ? (
                [
                  !!views && (
                    <>
                      <EyeIcon />
                      <Typography>{views}</Typography>
                    </>
                  ),
                  !!rating && (
                    <>
                      <StyledStarIcon />
                      {rating.toFixed(1)}
                    </>
                  ),
                ].map((item, index) => (
                  <Box
                    sx={{ display: 'flex', columnGap: 2, alignItems: 'center' }}
                    key={index}
                  >
                    {item}
                  </Box>
                ))
              ) : (
                <Skeleton variant="text" sx={{ width: '100%' }} />
              )}
            </Box>
          </Box>
        }
      />
    </CardActionArea>
  </Card>
);
