import {
  Sorting,
  SortingOptions,
  TopVariant,
  defaultSorting,
  defaultTopVariant,
  getSortingData,
} from 'screens/TopChords/types';
import { assign, createMachine } from 'xstate';

export const topChordsMachine = createMachine(
  {
    /** @xstate-layout N4IgpgJg5mDOIC5QBcD2AHAtAYwBaoCcJYBiAZQFEAVAfSoHkAFGgNQEEAlASTYDkqA2gAYAuolDpUsAJbJpqAHbiQAD0QA2AMwAaEAE9EmAIxGALAF9LuhaghxlaLHkLFlkmXMXK1CTAFY-ADo-XQNfACYLS3MgA */
    id: 'top-chords',

    predictableActionArguments: true,

    context: {
      selectedTopVariant: defaultTopVariant,
      selectedSortingOption: defaultSorting[defaultTopVariant],
      selectedSortingData: getSortingData(
        defaultTopVariant,
        defaultSorting[defaultTopVariant],
      ),
    },

    tsTypes: {} as import('./topChords.machine.typegen').Typegen0,

    schema: {
      events: {} as
        | {
            type: 'SET_TOP_VARIANT';
            topVariant: TopVariant;
          }
        | {
            type: 'SET_SORTING_OPTION';
            sortingOption: SortingOptions;
          },
      context: {} as {
        selectedTopVariant: TopVariant;
        selectedSortingOption: SortingOptions;
        selectedSortingData: Sorting[TopVariant][number];
      },
    },

    on: {
      SET_TOP_VARIANT: {
        actions: ['setTopVariant', 'getSelectedSortingData'],
      },
      SET_SORTING_OPTION: {
        actions: ['setSortingOption', 'getSelectedSortingData'],
      },
    },
  },
  {
    actions: {
      setTopVariant: assign({
        selectedTopVariant: (_context, event) => event.topVariant,
      }),
      setSortingOption: assign({
        selectedSortingOption: (_context, event) => event.sortingOption,
      }),
      getSelectedSortingData: assign({
        selectedSortingData: context =>
          getSortingData(
            context.selectedTopVariant,
            context.selectedSortingOption,
          ),
      }),
    },
  },
);
