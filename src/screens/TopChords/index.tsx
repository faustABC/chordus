import { FC } from 'react';
import { TopSelector } from './TopSelector';
import { Sorting as SortingComponent } from './Sorting';
import { Cards } from 'screens/TopChords/Cards';
import { HydrationBoundary, dehydrate } from '@tanstack/react-query';
import getQueryClient from 'utils/getQueryClient';
import { usePrefetchSongs } from 'queries/songs';
import { CenterDesktopLayout } from 'layouts/DesktopLayout/CenterDesktopLayout';
import { Box } from '@mui/material';
import { CreateSong } from 'screens/TopChords/CreateSong';

export const TopChords: FC = async () => {
  // const router = useRouter();
  // const [currentTop, setCurrentTop] = useState<TopVariant>(TopVariant.chord);
  // const [currentSorting, setCurrentSorting] = useState<SortingOptions>(
  //   SortingOptions.views,
  // );
  // const bottomRef = useRef<HTMLElement>(null);
  // const isBottomVisible = useIsOnScreen(bottomRef);

  // const currentSortingData = useMemo(
  //   () => sorting[currentTop].find(({ key }) => key === currentSorting),
  //   [currentSorting, currentTop],
  // );

  // const currentQuery = useMemo(
  //   () =>
  //     ({
  //       [TopVariant.chord]: songsQuery,
  //       [TopVariant.performer]: performersQuery,
  //     })[currentTop],
  //   [currentTop, performersQuery, songsQuery],
  // );

  // const isAnyLoading = useMemo(
  //   () => currentQuery.isLoading || currentQuery.isFetchingNextPage,
  //   [currentQuery.isFetchingNextPage, currentQuery.isLoading],
  // );

  // useEffect(() => {
  //   if (!currentSortingData) return;
  //   currentQuery.refetch();
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [currentSortingData]);

  // useEffect(() => {
  //   if (!currentSortingData) {
  //     setCurrentSorting(defaultSorting[currentTop]);
  //   }
  // }, [currentTop, currentSortingData]);

  // const onItemClick = useCallback(
  //   ({ id }: Song | Performer) =>
  //     router.push(
  //       currentTop === TopVariant.chord
  //         ? generatePath.Chords(id as Song['id'])
  //         : generatePath.Performer(id as Performer['id']),
  //     ),

  //   [currentTop, router],
  // );

  // const nextParams = useMemo(
  //   () => getNextPageParam(currentQuery.data?.pages),
  //   [currentQuery.data],
  // );

  // const onLoadMore = useCallback(() => {
  //   currentQuery.fetchNextPage({ pageParam: nextParams });
  // }, [currentQuery, nextParams]);

  // useEffect(() => {
  //   if (isBottomVisible && nextParams) {
  //     onLoadMore();
  //   }
  // }, [isBottomVisible, nextParams, onLoadMore]);

  const queryClient = getQueryClient();
  // eslint-disable-next-line react-hooks/rules-of-hooks
  await usePrefetchSongs();

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <CenterDesktopLayout>
        <Box
          component="main"
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            position: 'relative',
          }}
        >
          <TopSelector />
          <SortingComponent />
        </Box>
        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: { md: '1fr 1fr', xs: '1fr' },
            gap: 6,
            mt: 6,
          }}
        >
          <Cards />
        </Box>
        {/* <span ref={bottomRef} /> */}
        <CreateSong />
      </CenterDesktopLayout>
    </HydrationBoundary>
  );
};
