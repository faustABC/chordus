import * as YUP from 'yup';

export const validationSchema = YUP.object().shape({
  rhythm: YUP.array().of(
    YUP.number()
      .min(1, 'Rhythm must be from 1 to 32')
      .max(32, 'Rhythm must be from 1 to 32'),
  ),
  songTitle: YUP.string().min(2, 'Song title must be at least 2 characters'),
  performerBandName: YUP.string().min(
    2,
    'Performer must be at least 2 characters',
  ),
  titles: YUP.array().of(
    YUP.string()
      .min(2, ({ path }) => {
        const regex = /\[(?<sectionNumber>.*)\]/;
        const regexResult = regex.exec(path);
        const sectionNumber = regexResult?.groups?.sectionNumber;

        return (
          sectionNumber !== undefined &&
          `Title of section ${+sectionNumber + 1} must be at least 2 characters`
        );
      })
      .max(100),
  ),
});
