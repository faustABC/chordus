import { Box, Typography, Skeleton } from '@mui/material';
import { StyledStarIcon } from 'components/StyledStarIcon';
import { FC } from 'react';
import EyeIcon from '@/public/icons/eye.svg';

interface StatisticsProps {
  views?: number;
  rating?: number;
}

export const Statistics: FC<StatisticsProps> = ({ views, rating }) => {
  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      {typeof views === 'number' && typeof rating === 'number' ? (
        [
          <>
            <EyeIcon />
            <Typography variant="body2" sx={{ width: 40 }}>
              {views}
            </Typography>
          </>,
          <>
            <StyledStarIcon />
            <Typography variant="body2">{rating.toFixed(1)}</Typography>
          </>,
        ].map((item, index) => (
          <Box
            sx={{
              display: 'flex',
              columnGap: 2,
              alignItems: 'center',
            }}
            key={index}
          >
            {item}
          </Box>
        ))
      ) : (
        <Skeleton variant="text" sx={{ width: '100%' }} />
      )}
    </Box>
  );
};
