'use client';

import { ChordPanel } from 'components/ChordPanel';
import { RhythmInput } from 'components/RhythmInput';
import { FC, useCallback, useEffect, useMemo, useState } from 'react';
import {
  useIncrementSongViews,
  useSong,
  useSongVariants,
  useUpdateUserChordRating,
  useUserChordRating,
} from 'queries/songs';

import { Rating } from '../../components/Rating';
import { componentToPath, generatePath } from 'routing';
import { useParams, useRouter } from 'next/navigation';
import {
  Box,
  Button,
  Divider,
  MenuItem,
  TextField,
  Typography,
} from '@mui/material';
import { Breadcrumbs, links } from 'components/Breadcrumbs';
import { CenterDesktopLayout } from 'layouts/DesktopLayout/CenterDesktopLayout';
import { AdjustButtons } from 'screens/Chords/AdjustButtons';
import { Statistics } from 'screens/Chords/Statistics';
import { Actions } from 'screens/Chords/Actions';
import { useUserData } from 'queries/auth';

export const Chords: FC = () => {
  const router = useRouter();
  const routerParams = useParams();
  const songId = parseInt(String(routerParams.songId), 10);
  const song = useSong(songId);
  const rating = useUserChordRating(songId);
  const incrementSongViews = useIncrementSongViews();
  const songVariants = useSongVariants(songId);
  const updateUserChordRating = useUpdateUserChordRating();
  const auth = useUserData();

  const rateChords = async (rating: number) =>
    updateUserChordRating.mutateAsync({ songId, rating });

  useEffect(() => {
    if (songId) {
      incrementSongViews.mutateAsync(songId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [songId]);

  useEffect(() => {
    if (song.error || isNaN(songId)) {
      router.push(componentToPath.TopChords);
    }
  });

  const generatePanel = useCallback(
    (index: number) => (
      <ChordPanel
        key={index}
        rhythmLines={song.data?.rhythmPanels[index] ?? [[[]]]}
        wordLines={song.data?.chordPanels[index] ?? [[]]}
        title={song.data?.titles[index] ?? ''}
        rhythm={song.data?.rhythm ?? [0, 0]}
      />
    ),
    [song],
  );

  const onEditChordsClick = useCallback(() => {
    router.push(generatePath.SubmitChords(songId));
  }, [router, songId]);

  const [chordVersionId, setChordVersionId] = useState<number>();
  const handleChordVersionChange: React.ChangeEventHandler<
    HTMLTextAreaElement | HTMLInputElement
  > = useCallback(
    e => {
      setChordVersionId(Number(e.target.value));
      router.push(generatePath.Chords(Number(e.target.value)));
    },
    [router],
  );

  useEffect(() => {
    setChordVersionId(songId);
  }, [songId]);

  const chordVersionData = useMemo(
    () => songVariants.data?.find(({ id }) => id === chordVersionId),
    [chordVersionId, songVariants.data],
  );

  const isCurrentUserCreator = useMemo(
    () => song.data?.author.id === auth.data?.id,
    [auth.data?.id, song.data?.author.id],
  );

  const options = songVariants.data?.map((song, index, array) => (
    <MenuItem
      value={song.id}
      key={song.id}
      divider={index !== array.length - 1}
    >
      <Box sx={{ mr: 'auto' }}>{song.author.username}</Box>
      <Statistics rating={song.averageRating} views={song.views} />
    </MenuItem>
  ));

  const chordVariantSelect = (songVariants.data?.length ?? 0) > 1 && (
    <TextField
      select
      label="Versions"
      value={chordVersionId}
      onChange={handleChordVersionChange}
      sx={{ width: 360 }}
      SelectProps={{
        renderValue: () => (
          <div>
            <div>
              <span>Author: </span>
              <span>{chordVersionData?.author?.username}</span>
            </div>
          </div>
        ),
      }}
    >
      {options}
    </TextField>
  );

  const renderedRating = (
    <Box sx={{ gridColumnStart: 2 }}>
      <Typography marginBottom={2}>My rate:</Typography>
      <Rating
        disabled={updateUserChordRating.isPending || rating.isLoading}
        rating={rating.data?.current?.value ?? 0}
        setRating={rateChords}
        isLoading={rating.isLoading}
      />
    </Box>
  );

  const chords = (
    <>
      <Typography variant="h4">
        {song.data?.songTitle} - {song.data?.performer.bandName}
      </Typography>
      <Actions isCurrentUserCreator={isCurrentUserCreator} />
      {chordVariantSelect}
      <Statistics rating={song.data?.averageRating} views={song.data?.views} />
      <Box sx={{ display: 'flex', alignItems: 'center', gap: 12 }}>
        <Box component="aside" sx={{ display: 'flex', alignItems: 'center' }}>
          Time Signature
          <RhythmInput rhythm={song.data?.rhythm ?? [0, 0]} />
        </Box>
        <AdjustButtons>Tone</AdjustButtons>
        <AdjustButtons>Font Size</AdjustButtons>
      </Box>
      <section>
        {song.data?.rhythmPanels.map((_rhythmPanel, panelIndex) =>
          generatePanel(panelIndex),
        )}
      </section>
      {renderedRating}
    </>
  );

  return (
    <Box sx={{ width: '100%', mt: -6 }}>
      <CenterDesktopLayout
        ChildBoxProps={{
          sx: {
            py: 4,
          },
        }}
      >
        <Breadcrumbs
          links={[
            links.mainPage,
            {
              showSkeleton: !song.data?.songTitle,
              label: `${song.data?.songTitle} - ${song.data?.performer.bandName}`,
            },
          ]}
        />
      </CenterDesktopLayout>
      <Divider />
      <CenterDesktopLayout>
        <Box
          component="main"
          sx={{ display: 'flex', flexDirection: 'column', gap: 6, pt: 6 }}
        >
          {chords}
        </Box>
      </CenterDesktopLayout>
    </Box>
  );
};
