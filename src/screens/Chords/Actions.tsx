import { Box, Button, Typography } from '@mui/material';
import { FC, ReactElement } from 'react';
import HeartIcon from '@/public/icons/heart.svg';
import ShareIcon from '@/public/icons/share.svg';
import EditIcon from '@/public/icons/edit.svg';
import ReportProblemIcon from '@/public/icons/reportProblem.svg';

interface ActionsProps {
  isCurrentUserCreator?: boolean;
}

export const Actions: FC<ActionsProps> = ({ isCurrentUserCreator }) => {
  const actions = [
    {
      icon: HeartIcon,
      label: 'Save to your Profile',
    },
    {
      icon: ShareIcon,
      label: 'Share',
    },
    {
      icon: ReportProblemIcon,
      label: 'Report a Problem',
    },
    {
      icon: EditIcon,
      label: 'Edit',
      isHidden: !isCurrentUserCreator,
    },
  ] satisfies {
    icon: ReactElement;
    label: string;
    onClick?: () => void;
    isHidden?: boolean;
  }[];

  return (
    <Box sx={{ display: 'flex', gap: 6, alignItems: 'center' }}>
      {actions.map(
        ({ icon: Icon, label, isHidden }) =>
          !isHidden && (
            <Button
              key={label}
              sx={{ display: 'flex', gap: 2, textTransform: 'none' }}
              startIcon={<Icon />}
            >
              <Typography variant="body2">{label}</Typography>
            </Button>
          ),
      )}
    </Box>
  );
};
