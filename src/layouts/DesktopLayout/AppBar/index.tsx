'use client';

import { styled } from '@mui/material/styles';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import { useUserData } from 'queries/auth';
import { FC } from 'react';
import { useBoolean } from 'utils/hooks';
import { AccountButton } from 'layouts/DesktopLayout/AccountButton';
import { LoginButton } from 'layouts/DesktopLayout/LoadingButton';

export const Main = styled('main', {
  shouldForwardProp: prop => prop !== 'open',
})<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  padding: theme.spacing(3),
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginLeft: `-${drawerWidth}px`,
  ...(open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  }),
}));

export const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const drawerWidth = 240;

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const StyledAppBar = styled(MuiAppBar, {
  shouldForwardProp: prop => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  '&.MuiPaper-root': {
    paddingLeft: 0,
    paddingRight: 0,
  },
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

export const AppBar: FC<
  AppBarProps & {
    drawer: JSX.Element;
  }
> = ({ children, drawer, ...rest }) => {
  drawer;
  const [isDrawerOpened, { off: closeDrawer }] = useBoolean();
  closeDrawer;
  return (
    <>
      <StyledAppBar open={isDrawerOpened} {...rest}>
        {children}
      </StyledAppBar>
      {/* <Drawer
        variant="persistent"
        open={isDrawerOpened}
        onClose={closeDrawer}
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
      >
        {drawer}
      </Drawer> */}
    </>
  );
};

export const LoginServerButton: FC<{
  logInWithGoogleHref: string;
}> = ({ logInWithGoogleHref }) => {
  const userData = useUserData();

  return userData.data?.id ? (
    <AccountButton />
  ) : (
    <LoginButton logInWithGoogleHref={logInWithGoogleHref} />
  );
};
