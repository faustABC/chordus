'use client';

import { FC } from 'react';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import { Button } from '@mui/material';
import { useUserData } from 'queries/auth';
import Link from 'next/link';

export const AccountButton: FC = () => {
  const userData = useUserData();

  return (
    <>
      <Tooltip title="Profile">
        <Button
          LinkComponent={Link}
          href="/profile"
          sx={{
            display: 'flex',
            alignItems: 'center',
            textAlign: 'center',
            textTransform: 'none',
          }}
        >
          <Avatar src={userData.data?.picture} sx={{ w: 8, h: 8, mr: 1 }}>
            {userData.data?.username?.[0].toUpperCase()}
          </Avatar>
          <Typography sx={{ minWidth: 100 }} variant="bold">
            {userData.data?.username}
          </Typography>
        </Button>
      </Tooltip>
    </>
  );
};
