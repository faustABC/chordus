import { Box, Typography } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import React, { FC } from 'react';
import ProfileCircleIcon from '@/public/icons/profileCircle.svg';
import ProfileCircleFilledIcon from '@/public/icons/profileCircle.filled.svg';
import HomeIcon from '@/public/icons/home.svg';
import HomeFilledIcon from '@/public/icons/home.filled.svg';
import ExitIcon from '@/public/icons/exit.svg';
import SearchIcon from '@/public/icons/search.svg';
import CloseIcon from '@/public/icons/close.svg';
import PlusIcon from '@/public/icons/plus.svg';
import MinusIcon from '@/public/icons/minus.svg';
import TrashIcon from '@/public/icons/trash.svg';
import MusicIcon from '@/public/icons/music.svg';
import CopyIcon from '@/public/icons/copy.svg';
import CameraIcon from '@/public/icons/camera.svg';
import ArrowUpIcon from '@/public/icons/arrowUp.svg';
import CheckIcon from '@/public/icons/check.svg';
import ArrowLeftIcon from '@/public/icons/arrowLeft.svg';
import EditIcon from '@/public/icons/edit.svg';
import ShareIcon from '@/public/icons/share.svg';
import ReportProblemIcon from '@/public/icons/reportProblem.svg';
import StarIcon from '@/public/icons/star.svg';
import MoveIcon from '@/public/icons/move.svg';
import LinkIcon from '@/public/icons/link.svg';
import EyeIcon from '@/public/icons/eye.svg';
import WhatsappIcon from '@/public/icons/logos/whatsapp.svg';
import TelegramIcon from '@/public/icons/logos/telegram.svg';
import TwitterIcon from '@/public/icons/logos/twitter.svg';
import InstagramIcon from '@/public/icons/logos/instagram.svg';
import FacebookIcon from '@/public/icons/logos/facebook.svg';

interface IconProps {
  icon: React.ComponentType;
}

const Icon: FC<IconProps> = ({ icon: Icon }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: theme => theme.spacing(2),
      }}
    >
      <Icon />
      <Typography variant="caption">{Icon.name}</Typography>
    </Box>
  );
};

const meta: Meta<IconProps> = {
  component: Icon,
  title: 'UI Kit/Icons',
};

export default meta;

type Story = StoryObj<typeof Icon>;

export const Demo: Story = {
  render: () => (
    <Box
      sx={{
        display: 'flex',
        gap: theme => theme.spacing(10),
        flexWrap: 'wrap',
      }}
    >
      {[
        ProfileCircleIcon,
        ProfileCircleFilledIcon,
        HomeIcon,
        HomeFilledIcon,
        ExitIcon,
        SearchIcon,
        CloseIcon,
        PlusIcon,
        MinusIcon,
        TrashIcon,
        MusicIcon,
        CopyIcon,
        CameraIcon,
        ArrowUpIcon,
        CheckIcon,
        ArrowLeftIcon,
        EditIcon,
        ShareIcon,
        ReportProblemIcon,
        StarIcon,
        MoveIcon,
        LinkIcon,
        EyeIcon,
        WhatsappIcon,
        TelegramIcon,
        TwitterIcon,
        InstagramIcon,
        FacebookIcon,
      ].map((icon, index) => (
        <Icon key={index} icon={icon} />
      ))}
    </Box>
  ),
};
