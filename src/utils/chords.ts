import { Chord, ChordKeys, RhythmLine } from 'types';

export const createRhythmId = (tactIndex: number, chordIndex: number) =>
  [tactIndex, chordIndex].join();

const chordKeysGroup = `[${Object.values(ChordKeys).join('|')}]`;
const chordsRegex = new RegExp(
  `^(?<key>${chordKeysGroup})(?<body>.*)(?<tonic>${chordKeysGroup})?$`,
);

export const parseChordFromString = (
  stringifiedChord: string,
): Chord | null => {
  const res = stringifiedChord.match(chordsRegex);
  return (res?.groups as { key: ChordKeys }) ?? null;
};

export const getRawChordIndex = (
  tactIndex: number,
  chordIndex: number,
  rhythmLine: RhythmLine,
) =>
  rhythmLine.reduce(
    (acc, tact, i) => acc + (tactIndex > i ? tact.length : 0),
    chordIndex,
  );

export const generateRhythm = (columnsNumber: number) =>
  Array(columnsNumber + 1).fill(null);

export const stringifyChord = ({ key, body, tonic }: Chord) =>
  `${key}${body ?? ''}${tonic ? `/${tonic}` : ''}`;
