import { Box, Typography } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { FC } from 'react';
import { colors } from 'utils/theme';

interface PaletteBoxProps {
  color: keyof typeof colors;
}

const PaletteBox: FC<PaletteBoxProps> = ({ color }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: theme => theme.spacing(2),
      }}
    >
      <Box
        sx={{
          width: theme => theme.spacing(20),
          height: theme => theme.spacing(20),
          bgcolor: colors[color],
          borderRadius: theme => theme.spacing(2),
          boxShadow: theme => theme.shadows[10],
        }}
      />
      <Typography variant="caption">{color}</Typography>
    </Box>
  );
};

const meta: Meta<PaletteBoxProps> = {
  component: PaletteBox,
  title: 'UI Kit/Palette',
};

export default meta;

type Story = StoryObj<typeof PaletteBox>;

export const Demo: Story = {
  render: () => (
    <Box
      sx={{
        display: 'flex',
        gap: theme => theme.spacing(10),
        flexWrap: 'wrap',
      }}
    >
      <PaletteBox color="white" />
      <PaletteBox color="lightGrey" />
      <PaletteBox color="grey" />
      <PaletteBox color="darkGrey" />
      <PaletteBox color="black" />
      <PaletteBox color="green" />
      <PaletteBox color="red" />
      <PaletteBox color="purple" />
    </Box>
  ),
};
