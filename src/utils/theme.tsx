'use client';

import { InputBaseProps } from '@mui/material';
import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import { montserrat } from 'utils/fonts';
import { LinkBehavior } from 'utils/LinkBehavior';
import ArrowUpIcon from '@/public/icons/arrowUp.svg';

const inputSizeToHeight = {
  medium: 14,
  small: 11,
} satisfies Record<NonNullable<InputBaseProps['size']>, number>;

export const colors = {
  white: '#ffffff',
  lightGrey: '#f9f9f9',
  grey: '#eaeaea',
  darkGrey: '#737373',
  black: '#19323d',
  green: '#95e9b5',
  red: '#ffbfb9',
  purple: '#5d3cf2',
} as const;

declare module '@mui/material/styles' {
  interface TypographyVariants {
    bold: React.CSSProperties;
  }

  interface TypographyVariantsOptions {
    bold?: React.CSSProperties;
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    bold: true;
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsSizeOverrides {
    large: true;
    small: true;
    medium: false;
  }
}

const theme = createTheme({
  spacing: 4,
  palette: {
    mode: 'light',
    primary: {
      main: colors.black,
    },
    secondary: {
      main: colors.grey,
    },
    error: {
      main: colors.red,
    },
    success: {
      main: colors.green,
    },
    text: {
      primary: colors.black,
      secondary: colors.darkGrey,
    },
    background: {
      default: colors.white,
      paper: colors.lightGrey,
    },
  },
  typography: {
    fontFamily: montserrat.style.fontFamily,
    fontSize: 14,
    h1: {
      fontWeight: 600,
      wordWrap: 'break-word',
    },
    h2: {
      fontWeight: 600,
      wordWrap: 'break-word',
    },
    h3: {
      fontWeight: 600,
      wordWrap: 'break-word',
    },
    h4: {
      fontWeight: 600,
      letterSpacing: 0.75,
      wordWrap: 'break-word',
    },
    body1: {
      fontWeight: 400,
      letterSpacing: 0.5,
      wordWrap: 'break-word',
    },
    bold: {
      fontWeight: 600,
      letterSpacing: 0.25,
      wordWrap: 'break-word',
    },
    body2: {
      fontWeight: 400,
      letterSpacing: 0.25,
      wordWrap: 'break-word',
    },
    caption: {
      fontWeight: 400,
      letterSpacing: 0.4,
      wordWrap: 'break-word',
    },
  },
  components: {
    MuiLink: {
      defaultProps: {
        component: LinkBehavior,
      },
    },
    MuiTypography: {
      defaultProps: {
        variantMapping: {
          bold: 'span',
        },
      },
    },
    MuiButtonGroup: {
      styleOverrides: {
        grouped: ({ theme }) => ({
          minWidth: theme.spacing(36),
          textTransform: 'capitalize',
          height: 44,
          ...theme.typography.body1,
        }),
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: ({ theme, ownerState }) => ({
          height: theme.spacing(inputSizeToHeight[ownerState.size ?? 'medium']),
        }),
      },
    },
    MuiButtonBase: {
      defaultProps: {
        LinkComponent: LinkBehavior,
      },
    },
    MuiButton: {
      styleOverrides: {
        root: ({ ownerState, theme }) => {
          const sizeToStyle = {
            large: {
              padding: [theme.spacing(4), theme.spacing(6)].join(' '),
              height: theme.spacing(14),
              minWidth: 328,
              maxWidth: 380,
            },
            small: {
              padding: [theme.spacing(2), theme.spacing(6)].join(' '),
            },
          } as const;

          return {
            textTransform: 'none',
            borderRadius: theme.spacing(14),
            ...sizeToStyle[ownerState.size!],
          };
        },
      },
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          borderRadius: 24,
          padding: 24,
        },
        paperWidthXs: {
          maxWidth: 380,
        },
      },
    },
    MuiMenu: {
      styleOverrides: {
        paper: {
          background: 'white',
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          background: 'white',
        },
      },
    },
    MuiAutocomplete: {
      defaultProps: {
        popupIcon: <ArrowUpIcon style={{ transform: 'rotate(180deg)' }} />,
      },
      styleOverrides: {
        listbox: {
          background: 'white',
        },
      },
    },
  },
});

export default responsiveFontSizes(theme);
