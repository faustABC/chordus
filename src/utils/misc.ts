import { ChangeEvent, Dispatch, SetStateAction, useCallback } from 'react';

export const selectAllTextInElement = (element: HTMLElement) => {
  const selection = window.getSelection();
  const range = document.createRange();
  range.selectNodeContents(element);
  selection?.removeAllRanges();
  selection?.addRange(range);
};

export const useOnChange = (setValue: Dispatch<SetStateAction<string>>) =>
  useCallback(
    () => (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.value),
    [setValue],
  );
