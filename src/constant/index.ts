export enum DndTypes {
  CHORD_ELEMENT = 'CHORD_ELEMENT',
}

export const fallbackAvatar =
  'https://react.semantic-ui.com/images/avatar/large/matthew.png';

/**
 * 12 Rows with 2 cards on each
 */
export const DEFAULT_PAGE_SIZE = 12 * 2;

export const AUTOCOMPLETE_DEFAULT_ITEMS_SIZE = 4;

export const breakpoints = {
  xs: 0,
  sm: 768,
  md: 1000,
  lg: 1200,
} as const;

export const breakpointsEntries = Object.entries(breakpoints).sort(
  (a, b) => a[1] - b[1],
) as [keyof typeof breakpoints, number][];
