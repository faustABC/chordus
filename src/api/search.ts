import { Performer, Song } from 'types';
import { apiGatewayClient } from './clients';

export interface GlobalSearchDto {
  songs: {
    id: Song['id'];
    songTitle: Song['songTitle'];
    views: Song['views'];
    performer: {
      id: Performer['id'];
      bandName: Performer['bandName'];
    };
  }[];
  performers: {
    id: Performer['id'];
    bandName: Performer['bandName'];
    views: number;
  }[];
}

export const globalSearch = async (
  filter: string,
): Promise<GlobalSearchDto> => {
  const { data } = await apiGatewayClient.get<GlobalSearchDto>(
    '/search/global',
    { params: { filter } },
  );
  return data;
};
