import { apiGatewayClient } from './clients';

export interface LoggedInUser {
  id: string;
  username: string;
  picture: string;
  email: string;
  locale: string;
}

export const getUserData = async () => {
  const { data } = await apiGatewayClient.get<LoggedInUser>('/auth', {
    withCredentials: true,
  });
  return data;
};

export const logOut = async () => apiGatewayClient.post<never>('/auth/log-out');
