import * as users from './users';
import * as auth from './auth';
import * as songs from './songs';
import * as performers from './performers';
import * as search from './search';
import * as dtos from './dtos';

const api = {
  users,
  auth,
  songs,
  performers,
  search,
  dtos,
};

export default api;
