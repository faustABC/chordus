import { User } from 'types';
import { apiGatewayClient } from './clients';

export const getAll = async () => {
  const { data } = await apiGatewayClient.get<User[]>('/users');
  return data;
};
