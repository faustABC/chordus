import { Performer, Rating, Song } from 'types';

export interface CreateSongDto
  extends Pick<
    Song,
    'chordPanels' | 'rhythm' | 'rhythmPanels' | 'songTitle' | 'titles'
  > {
  performerBandName: string;
}

export interface FetchItemsDto<T> {
  top?: number;
  skip?: number;
  filter?: string;
  orderBy?: keyof T;
}

export interface FetchItemsResponseDto<T> {
  totalCount: number;
  items: T[];
}

export type FetchedPerformer = Performer & { highlightedSong: string };
export type FetchedSong = Song;

export type FetchSongsResponseDto = FetchItemsResponseDto<FetchedSong>;
export type FetchPerformersResponseDto =
  FetchItemsResponseDto<FetchedPerformer>;

export interface RateSongDto {
  songId: Song['id'];
  rating: number;
}

export interface GetRatingDto {
  current?: Rating;
  average?: number;
}
