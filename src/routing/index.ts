import { Performer, Song } from 'types';

export const componentToPath = {
  TopChords: '/',
  SubmitSong: '/submit-chords/:songId',
  NotFound: '/not-found',
  Chords: '/song/:songId',
  Performer: '/performer/:performerId',
} as const;

export const generatePath = {
  Chords: (songId: Song['id']) => `/song/${songId}`,
  SubmitSong: (songId?: Song['id']) => `/submit-chords/${songId ?? ''}`,
  Performer: (performerId: Performer['id']) => `/performer/${performerId}`,
} as const;
