import { Box, Skeleton } from '@mui/material';
import { CenterDesktopLayout } from 'layouts/DesktopLayout/CenterDesktopLayout';

export default function Loading() {
  return (
    <CenterDesktopLayout>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Skeleton variant="rounded" sx={{ width: 287, height: 44 }} />
        <Skeleton variant="rounded" sx={{ width: 120, height: 44 }} />
      </Box>
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: { md: '1fr 1fr', xs: '1fr' },
          gap: 6,
          mt: 6,
        }}
      >
        {Array.from({ length: 16 }).map((_, index) => (
          <Skeleton key={index} variant="rounded" sx={{ height: 92 }} />
        ))}
      </Box>
    </CenterDesktopLayout>
  );
}
