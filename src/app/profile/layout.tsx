'use client';

import { redirect } from 'next/navigation';
import { useUserData } from 'queries/auth';
import { ReactNode } from 'react';

export default function Layout({
  authenticated,
  children,
}: {
  authenticated: ReactNode;
  children: ReactNode;
}) {
  const userData = useUserData();

  if (!userData.isFetched) {
    return children;
  }

  if (userData.isFetched && !userData.data?.id) {
    redirect('/');
  }

  return (
    <>
      {authenticated}
      {children}
    </>
  );
}
