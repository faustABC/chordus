import { Metadata } from 'next';
import theme from 'utils/theme';
import Providers from 'providers';
import { ResponsiveContainer } from 'layouts/ResponsiveContainer';
import { montserrat } from 'utils/fonts';

export const metadata: Metadata = {
  title: 'ChorDm',
  description: 'Song chords online',
  icons: {
    icon: '/favicon.ico',
    apple: '/logo192.png',
  },
  manifest: '/manifest.json',
  themeColor: theme.palette?.primary?.main,
  viewport: 'width=device-width, initial-scale=1',
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" className={montserrat.className}>
      <body>
        <Providers>
          <ResponsiveContainer>{children}</ResponsiveContainer>
        </Providers>
      </body>
    </html>
  );
}
