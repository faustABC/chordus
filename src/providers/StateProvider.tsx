import { PropsWithChildren, createContext } from 'react';
import { useInterpret } from '@xstate/react';
import { topChordsMachine } from 'screens/TopChords/topChords.machine';
import { InterpreterFrom } from 'xstate';

export const GlobalStateContext = createContext({
  topChords: {} as InterpreterFrom<typeof topChordsMachine>,
});

export const GlobalStateProvider = (props: PropsWithChildren) => {
  const topChords = useInterpret(topChordsMachine);

  return (
    <GlobalStateContext.Provider value={{ topChords }}>
      {props.children}
    </GlobalStateContext.Provider>
  );
};
