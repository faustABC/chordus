import { useActor } from '@xstate/react';
import { GlobalStateContext } from 'providers/StateProvider';
import { usePerformers } from 'queries/performer';
import { useSongs } from 'queries/songs';
import { useContext, useMemo } from 'react';
import { TopVariant } from 'screens/TopChords/types';
import { Performer, Song } from 'types';

export const useTopChordsSelectedQuery = () => {
  const globalServices = useContext(GlobalStateContext);
  const [topChordsState] = useActor(globalServices.topChords);
  const { selectedSortingData, selectedTopVariant } = topChordsState.context;

  const performersQuery = usePerformers(
    selectedSortingData.property as unknown as keyof Performer,
    selectedTopVariant,
  );

  const songsQuery = useSongs(
    selectedSortingData.property as unknown as keyof Song,
    selectedTopVariant,
  );

  const currentQuery = useMemo(
    () =>
      ({
        [TopVariant.chord]: songsQuery,
        [TopVariant.performer]: performersQuery,
      })[topChordsState.context.selectedTopVariant],
    [topChordsState.context.selectedTopVariant, performersQuery, songsQuery],
  );

  const isAnyLoading =
    currentQuery.isLoading || currentQuery.isFetchingNextPage;

  return { currentQuery, isAnyLoading, performersQuery, songsQuery };
};
