import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import API from 'api';
import { LoggedInUser } from 'api/auth';

export const useUserData = () =>
  useQuery<LoggedInUser | null>({
    queryFn: API.auth.getUserData,
    queryKey: ['user-data'],
  });

export const useLogOut = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: API.auth.logOut,
    onSuccess: () => {
      queryClient.setQueryData(['user-data'], null);
    },
  });
};
